# changelog

## 1.0.1

- added readme and changelog
- remove compiled js files from version control as these should be build on deployment

## 1.0.0

- initial