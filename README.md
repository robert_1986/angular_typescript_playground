# README #

## TypeScript / AngularJS / PHP Slim ##

Version 1.0.1

This is an example project to demo the use of TypeScript to build a simple AngularJS single page application. For demo purposes a simple PHP API is used to get data from a static file, this PHP API utilises the Slim micro framework.

All project dependancies are managed with NPM, Bower and Composer. TypeScript compilation is run on the command line with the TypeScript compiler (tsc) command. JavaScript code checking and minification are handled by gulp using gulp-jshint and gulp-uglify.

### Pre-requisits ###

* NodeJS, NPM, Bower and Composer installed globally on the working environment
* Apache/Nginx
* Access to web server configuration
* PHP 5.3.2+

### How do I get set up? ###

* git clone this repository
* `$ cd` to the root of the project
* run `$ npm install` to install required node_modules
* run `$ bower install` to install front end dependencies
* run `$ composer install` to install php components
* run `$ tsc --sourcemap --noImplicitAny --out js/Application.js js/_all.ts` to compile the TypeScript source to JavaScript
* run `$ gulp` to check and minify the compiled JavaScript
* setup your web server to serve the project. for apache setups this means creating a new hosts entry for this project and adding the ServerName to your local hosts file

View the live demo [here](http://angular-playground.rcurran.co.uk)