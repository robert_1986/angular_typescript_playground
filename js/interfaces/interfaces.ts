/// <reference path="../_all.ts" />

module angularPlayground.interfaces {

	// ------ Scopes ------ //
	export interface IScopePlayground extends ng.IScope {
		greeting: string;
		location: any;
		changeName(name: string): void;
	}

	export interface IScopeUsers extends ng.IScope {
		clearBtnText: string;
	}

	export interface IScopeUser extends ng.IScope {
		userId: number;
	}

	// ------ Services ------ //
	export interface IUsersService {
		getUsers( successFn: Function, errorFn: Function ): void;
		getUser( id: number, successFn: Function, errorFn: Function ): void;
	}

	// ------ Controllers ------ //
	export interface IUserCtrl {
		users: Object;
		loadUsers(): void;
		clearUsers(): void;
	}

	export interface IUserDetailCtrl {
		user: Object;
		loadUser( id: number ): void;
	}

	// ------ RouteParams ------ //
	export interface IUserRouteParams extends ng.route.IRouteParamsService {
		id: number;
	}
}
