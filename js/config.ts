/// <reference path="_all.ts" />

module angularPlayground.config {

	export class Config {

		static $inject = [
			"$routeProvider"
		];

		constructor( private $routeProvider: ng.route.IRouteProvider ) {
			$routeProvider
			.when("/users", {
				controller: angularPlayground.controllers.UsersCtrl,
				controllerAs: "mc",
				templateUrl: "partials/users-list.html"
			})
			.when("/users/:id", {
				controller: angularPlayground.controllers.UsersDetailCtrl,
				controllerAs: "mc",
				templateUrl: "partials/user-details.html"
			}).otherwise({
					redirectTo: "/"
			});
		}
	}
}
