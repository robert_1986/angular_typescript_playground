/// <reference path="../_all.ts" />

module angularPlayground.services {

	import IUsersService = angularPlayground.interfaces.IUsersService;

	export class UsersService implements IUsersService {

		public static factory() {
			return [
				"$http",
				($http: ng.IHttpService) => new UsersService($http)
			];
		}

		constructor( private $http: ng.IHttpService ) {
		}

		public getUsers( successFn: Function, errorFn: Function ): void {
			this.$http
				.get( "/api/users" )
					.success( ( data, status ) => {
						successFn(data);
					})
					.error( ( data, status ) => {
						errorFn(data);
					});
		}

		public getUser( id: number, successFn: Function, errorFn: Function ): void {
			this.$http
				.get( "/api/users/" + id )
					.success( ( data, status ) => {
						successFn(data);
					})
					.error( ( data, status ) => {
						errorFn(data);
					});
		}
	}
}
