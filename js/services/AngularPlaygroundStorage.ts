/// <reference path="../_all.ts" />

module angularPlayground.services {

	export class AngularPlaygroundStorage {

		public static factory() {
			return [
				() => new AngularPlaygroundStorage()
			];
		}

		constructor() {
		}

		public get(key: string): string {
			return JSON.parse(localStorage.getItem(key) || '""');
		}

		public set(key: string, val: any): void {
			localStorage.setItem(key, JSON.stringify(val));
		}
	}
}
