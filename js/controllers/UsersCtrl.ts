/// <reference path="../_all.ts" />

module angularPlayground.controllers {

	import IScopeUsers = angularPlayground.interfaces.IScopeUsers;
	import IUserCtrl = angularPlayground.interfaces.IUserCtrl;

	export class UsersCtrl implements IUserCtrl {

		public static $inject = [
			"$scope",
			"UsersService"
		];

		public users: Object = {};

		constructor(private $scope: IScopeUsers, private UsersService: angularPlayground.interfaces.IUsersService )  {

			$scope.clearBtnText = "Clear Users";
			this.loadUsers();
		}

		public loadUsers(): void {
			this.UsersService.getUsers((data: Object) => {
				if (data !== null) {
					this.users = data;
				}
			}, (data: Object) => {
				this.users = null;
			});
		}

		public clearUsers(): void {
			this.users = null;
		}
	}
}
