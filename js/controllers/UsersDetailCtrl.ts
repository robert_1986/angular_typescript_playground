/// <reference path="../_all.ts" />

module angularPlayground.controllers {

	import IScopeUser = angularPlayground.interfaces.IScopeUser;
	import IUserDetailCtrl = angularPlayground.interfaces.IUserDetailCtrl;
	import IUserRouteParams = angularPlayground.interfaces.IUserRouteParams;

	export class UsersDetailCtrl implements IUserDetailCtrl {

		public static $inject = [
			"$scope",
			"$routeParams",
			"UsersService"
		];

		public user: Object = {};

		constructor(private $scope: IScopeUser, private $routeParams: IUserRouteParams, private UsersService: angularPlayground.interfaces.IUsersService )  {

			$scope.userId = $routeParams.id;
			this.loadUser($scope.userId);

		}

		public loadUser( id: number ): void {
			this.UsersService.getUser( id, (data: Object) => {
				if (data !== null) {
					this.user = data;
				}
			}, (data:Object) => {
				this.user = null;
			});
		}
	}
}
