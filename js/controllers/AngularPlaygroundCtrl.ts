/// <reference path="../_all.ts" />

module angularPlayground.controllers {

	import IScopePlayground = angularPlayground.interfaces.IScopePlayground;

	export class AngularPlaygroundCtrl {

		public static factory() {
			return [
				"$scope",
				"$location",
				"AngularPlaygroundStorage",
				($scope: IScopePlayground, $location: ng.ILocationService, AngularPlaygroundStorage: angularPlayground.services.AngularPlaygroundStorage) => new AngularPlaygroundCtrl($scope, $location, AngularPlaygroundStorage)
			];
		}

		constructor(private $scope: IScopePlayground, private $location: ng.ILocationService, private AngularPlaygroundStorage: angularPlayground.services.AngularPlaygroundStorage) {

			$scope.greeting = AngularPlaygroundStorage.get("name") !== "" ? "Hello " + AngularPlaygroundStorage.get("name") + "!" : "Click Me!";

			$scope.changeName = (name) => {
				AngularPlaygroundStorage.set("name", name);
				$scope.greeting = "Hello " + name + "!";
			};

			$scope.$on("$locationChangeSuccess", () => {
				$scope.location = $location.path();
			});
		}
	}
}
