/// <reference path="_all.ts" />

module angularPlayground {

	export class Loader {

		public app: ng.IModule;

		constructor( name: string = "app", extMods: Array<string> = [] ) {
			this.init(name, extMods);
		}

		private init( appName: string, externalModules: Array<string> ) {
			// Create a new angular module, injecting any required external modules
			this.app = angular.module(appName, externalModules);
			// Automatically register all services, directive and controllers with a static factory function
			this.registerComponents(angularPlayground.services, this.app.service);
			this.registerComponents(angularPlayground.directives, this.app.directive);
			this.registerComponents(angularPlayground.controllers, this.app.controller);
			// Register the confog class that contains routing info
			this.app.config(angularPlayground.config.Config);
		}

		private registerComponents(components: any, registrar: Function ) {
			for( var name in components ) {
				var component = components[name];
				if( typeof component.factory === "function" ) {
					registrar(name, component.factory());
				}
			}
		}
	}
}

// Createa a new instance of Loader that creates out app and registers all correctly named/configured
var app = new angularPlayground.Loader("angularPlayground", ["ngRoute"]);
