/// <reference path="../bower_components/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../bower_components/DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../bower_components/DefinitelyTyped/angularjs/angular-route.d.ts" />

/// <reference path="interfaces/interfaces.ts" />

/// <reference path="services/AngularPlaygroundStorage.ts" />
/// <reference path="services/UsersService.ts" />

/// <reference path="controllers/AngularPlaygroundCtrl.ts" />
/// <reference path="controllers/UsersCtrl.ts" />
/// <reference path="controllers/UsersDetailCtrl.ts" />

/// <reference path="directives/ChangeName.ts" />

/// <reference path="config.ts" />
/// <reference path="app.ts" />
