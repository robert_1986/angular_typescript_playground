/// <reference path="../_all.ts" />

module angularPlayground.directives {

	import IScopePlayground = angularPlayground.interfaces.IScopePlayground;

	export class changeName implements ng.IDirective{

		public static factory() {
			return [
				() => {
					return {
						restrict: "A",
						scope: false,
						link: ($scope: IScopePlayground, element: JQuery ) => {
							element.on("mouseenter", function() {
								element.addClass("animate");
							})
							.on("mouseleave", function() {
								element.removeClass("animate");
							})
							.on("click", function() {
								var name = JSON.parse(JSON.stringify(prompt("Please enter your name")));
								$scope.changeName(name);
								$scope.$digest()
							});
						}
					}
				}
			];
		}
	}
}
