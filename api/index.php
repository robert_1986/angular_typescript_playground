<?php

require '../vendor/autoload.php';

$app = new \Slim\Slim();

$app->get( "/", function() {
	echo "<h1>Hi Slim!</h1>";
});

$app->get( "/users", function() use($app) {
	require 'mockUserData.php';
	$app->response()->setStatus(200);
	$app->response()->headers->set('Content-type', 'application/json');
	echo json_encode( $users, true );
});

$app->get( "/users/:id", function($id) use($app) {
	require 'mockUserData.php';

	if( is_numeric( $id ) ) {
		$user = array_key_exists( $id, $users ) ? $users[$id] : null;
	}

	if( !empty( $user ) ) {
		$app->response()->setStatus(200);
		$app->response()->headers->set('Content-type', 'application/json');
		echo json_encode( $user, true );
	}
	else {
		$app->response()->setStatus(404);
		$app->response()->headers->set('Content-type', 'application/json');
		echo json_encode( array( "error" => "User not found." ), true );
	}
});

$app->run();

?>
