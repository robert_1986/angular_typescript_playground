<?php

$users = array(

	1 => array(
		"id" => 1,
		"firstname" => "John",
		"lastname" => "Smith"
	),
	2 => array(
		"id" => 2,
		"firstname" => "Jo",
		"lastname" => "Bloggs"
	),
	3 => array(
		"id" => 3,
		"firstname" => "John",
		"lastname" => "Doe"
	),
	4 => array(
		"id" => 4,
		"firstname" => "Jane",
		"lastname" => "Doe"
	),
	5 => array(
		"id" => 5,
		"firstname" => "Rosie",
		"lastname" => "Morton"
	),
	6 => array(
		"id" => 6,
		"firstname" => "Peter",
		"lastname" => "Grant"
	)
);

?>