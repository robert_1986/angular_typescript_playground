var gulp = require('gulp');

var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('lint', function() {
    return gulp.src(['js/*.js','!js/*.min.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('minify', function() {
    return gulp.src('js/Application.js')
        .pipe(rename('Application.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js/'));
});

gulp.task('default', ['lint', 'minify']);
