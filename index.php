<!DOCTYPE html>
<html lang="en" ng-app="angularPlayground">
<head>
	<meta charset="utf-8">
	<title>Angular With TypeScript</title>

	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">

	<style type="text/css">
		.animate {
			background-color: #A03F9B;
			color: #ffffff;
		}
	</style>
</head>

<body ng-controller="AngularPlaygroundCtrl">
	<nav class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<a href="/" class="navbar-brand">AngularJS, TypeScript &amp; Slim</a>
			</div>
			<ul class="nav navbar-nav">
				<li ng-class="{'active': location.indexOf('/users')>=0}">
					<a href="/#/users">Users</a>
				</li>
			</ul>
		</div>
	</nav> <!-- end .navbar -->

	<div class="container">

		<div class="jumbotron">
			<h2 change-name>{{greeting}}</h2>
		</div>

		<div ng-view></div>

	</div> <!-- end .container -->

	<script src="bower_components/angular/angular.min.js"></script>
	<script src="bower_components/angular-route/angular-route.min.js"></script>
	<script src="js/Application.min.js"></script>
</body>
</html>
